<?php
    
    require_once 'sql_info.php';

    $connection = new mysqli($db_hostname, $db_username, $db_password, $db_database);
    if($connection->connect_error) die($connection->connect_error);

    function queryMysql($query)
    {
        global $connection;
        $result = $connection->query($query);
        if(!$result) die($connection->error);
        return $result;
    }
    
    function destroySession()
    {
        $_SESSION = array();
        
        if(session_id() != "" || isset($_COOKIE[session_name()]))
        {
            setcookie(session_name(), '', time()-2592000, '/');
        }
        session_destroy();
    }
    
    function generateRandomString($length) 
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) 
        {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
    
    function sendMail($address)
    {
        require_once("mailer/PHPMailerAutoload.php");   // 匯入PHPMailer類別       
        global $email_password;
        
        $tempRan = generateRandomString(20);
        $mail = new PHPMailer();                        // 建立新物件        

        $mail->IsSMTP();                                // 設定使用SMTP方式寄信        
        $mail->SMTPAuth = true;                         // 設定SMTP需要驗證

        $mail->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線   
        $mail->Host = "smtp.gmail.com";                 // Gmail的SMTP主機        
        $mail->Port = 465;                              // Gmail的SMTP主機的port為465      
        $mail->CharSet = "utf-8";                       // 設定郵件編碼   
        $mail->Encoding = "base64";
        $mail->WordWrap = 50;                           // 每50個字元自動斷行
        $mail->Username = "chalyWang0714@gmail.com";     // 設定驗證帳號        
        $mail->Password = $email_password;              // 設定驗證密碼        

        $mail->From = "chalyWang0714@gmail.com";         // 設定寄件者信箱        
        $mail->FromName = "CL";                 // 設定寄件者姓名        

        $mail->Subject = "驗證信";                     // 設定郵件標題        

        $mail->IsHTML(true);                            // 設定郵件內容為HTML        

        $mail->AddAddress($address, "user");
        $mail->Body = $mail->Body =                    // AddAddress(receiverMail, receiverName)
                "
                validation link:</br>
                http://localhost/SQA_final/app/email_validation.php?auth=" .$tempRan;
        
        if($mail->Send()) 
        {                             // 郵件成功寄出
            echo " Sended\n";
            $rows = queryMysql("SELECT * FROM user WHERE email = '$address'");
            $row = mysqli_fetch_assoc($rows);
            $userid = $row["userID"];
            
            queryMysql("INSERT INTO validation VALUES('$userid','$tempRan')");
        }
        else 
        {
            echo $mail->ErrorInfo . "<br>";
        }
        $mail->ClearAddresses();
    }
    
    function sendMailWithCode($address, $code)
    {
        require_once("mailer/PHPMailerAutoload.php");   // 匯入PHPMailer類別       
        global $email_password;
        
        $mail = new PHPMailer();                        // 建立新物件        
        $mail->IsSMTP();                                // 設定使用SMTP方式寄信        
        $mail->SMTPAuth = true;                         // 設定SMTP需要驗證

        $mail->SMTPSecure = "ssl";                      // Gmail的SMTP主機需要使用SSL連線   
        $mail->Host = "smtp.gmail.com";                 // Gmail的SMTP主機        
        $mail->Port = 465;                              // Gmail的SMTP主機的port為465      
        $mail->CharSet = "utf-8";                       // 設定郵件編碼   
        $mail->Encoding = "base64";
        $mail->WordWrap = 50;                           // 每50個字元自動斷行
        $mail->Username = "chalyWang0714@gmail.com";     // 設定驗證帳號        
        $mail->Password = $email_password;              // 設定驗證密碼        

        $mail->From = "chalyWang0714@gmail.com";         // 設定寄件者信箱        
        $mail->FromName = "CL";                 // 設定寄件者姓名        

        $mail->Subject = "重設密碼";                     // 設定郵件標題        

        $mail->IsHTML(true);                            // 設定郵件內容為HTML        

        $mail->AddAddress($address, "user");
        $mail->Body = $mail->Body =                    // AddAddress(receiverMail, receiverName)
                "
                validation link:</br>
                http://localhost/SQA_final/app/forgot_password_process.php?auth=" .$code;
        
        if($mail->Send()) 
        {                             // 郵件成功寄出
//            echo " Sended\n";
        }
        else 
        {
            echo $mail->ErrorInfo . "<br>";
        }
        $mail->ClearAddresses();
    }

