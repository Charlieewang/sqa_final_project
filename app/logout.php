<?php
require_once 'header.php';

unset($_SESSION['user']);
unset($_SESSION['userID']);
session_unset();

header("Location: ../index.php"); 

