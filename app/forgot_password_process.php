<?php
    require_once 'header.php';
    $uid = null;
    
    if(filter_has_var(INPUT_GET, 'auth'))
    {
        $authcode = filter_input(INPUT_GET, 'auth');
        $rows = queryMysql("SELECT * FROM forgot_password_request WHERE code = '$authcode'");
        if(mysqli_num_rows($rows) != 0)
        {
            $row = mysqli_fetch_assoc($rows);
            $uid = $row['userID'];
        }
        else
        {
            //wrong link
//            header("Location: ../index.php");
        }
    }
    else
    {
        //header("Location: ../index.php");
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Raleway|Poppins" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="../css/forgot_password.css">
        <script>
            function checkPassword()
            {
                var pass1 = document.getElementById("p1");
                var pass2 = document.getElementById("p2");
                if (pass1.value !== pass2.value) 
                {
                    pass2.setCustomValidity("wrong");
                }
                else
                {
                    pass2.setCustomValidity("");
                }
            }
        </script>
    </head>
    
    <body>
        <div class="title">
            <h1>reset</h1>
        </div>
        
        <div class="reset_form">
            <form action="reset_password.php" autocomplete="off" method="post">

                <?php if($uid != null) : ?>
                <input type="hidden" name="userID" value="<?php echo $uid; ?>">
                
                <input id="p1" type="password" name="Password" pattern=".{1,}" maxlength="20" required>
                <div class="label-text">New Password</div>

                <input id="p2" type="password" name="Password2" pattern=".{1,}" maxlength="20" onchange="checkPassword();" required>
                <div class="label-text">Password again</div>

                <button id="submit" type="submit">ENTER</button><br>
                <?php else: echo "Something went wrong."; endif; ?>
            </form>
        </div>
    </body>
</html>