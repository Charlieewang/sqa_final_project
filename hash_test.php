<?php

$ecrypt_password = password_hash("test", PASSWORD_DEFAULT);

echo $ecrypt_password;
echo "<br>";

$temp = '$2y$10$VWF9lrjsPSCXSA0owskNXOQyNHqVCYKF23Y6i5.Dvr85OpObpkTd6';

if (password_verify('test', $temp)) 
{
    echo 'Password is valid!';
} 
else 
{
    echo 'Invalid password.';
}