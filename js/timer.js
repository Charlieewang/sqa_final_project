var active = false;

function startTimer()
{
    var timerSetting;
    var timeArray;
    var minutes, seconds;
    
    if(active)
    {
        minutes = document.getElementById("min").value;
        seconds = document.getElementById("sec").value;
        
        //count down
        
        if(isNaN(minutes) || parseInt(minutes) < 0)
        {
            minutes = 0;
        }
        if(isNaN(seconds) || parseInt(seconds) < 0)
        {
            seconds = 5;
        }
        
        if(seconds > 60) seconds = 60;
        
        if(seconds == 0)
        {
            console.log("seconds == 0");
            if(minutes == 0)
            {
                active = false;
                document.getElementById("control_btn").innerHTML = "&#9654;";
//                document.getElementById("control_btn").disabled = true;
                minutes = "00";
            }
            else
            {
                minutes -- ;
                if(minutes < 10)
                {
                    minutes = "0" + minutes;
                    console.log("minutes < 10");
                }
                seconds = 59;
            }
        }
        else
        {   
            seconds -- ;
            if(seconds < 10)
            {   
                seconds = "0" + seconds;
                console.log("seconds < 10");
            }
            if(minutes == 0)
            {
                minutes = "00";
            }
        }
        
        
//        document.getElementById("min").innerHTML = minutes;
        document.getElementById("min").value = minutes;
        document.getElementById("sec").value = seconds;
        setTimeout(startTimer, 1000);
        console.log("setTimeout");
    }
}

function changeState()
{
    var minutes = document.getElementById("min").value;
    var seconds = document.getElementById("sec").value;
    
    if(active == false)
    {
        if(minutes != 0 || seconds != 0)
        {
            active = true;
            startTimer();
            console.log("timer start!");
            document.getElementById("control_btn").innerHTML = "&#10074;&#10074;";
        }
    }
    else
    {
        active = false;
        console.log("timer paused!");
        document.getElementById("control_btn").innerHTML = "&#9654;";
    }
}

function reset()
{
//    document.getElementById("timer").innerHTML = time;
    document.getElementById("min").value = '25';
    document.getElementById("sec").value = '00';
//    document.getElementById("control_btn").disabled = false;
}