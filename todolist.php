<!DOCTYPE html>
<?php
    require_once 'app/header.php';
    if($loggedIn == FALSE)
    {
        header("Location: index.php");
    }
    $results = queryMysql("SELECT * FROM todolist where userID = $userID order by done asc, listID desc");
    
    /*foreach ($results as $item) //取出$result內的每一個row
    {
        echo $item['todo'] . "<br>";//對應資料庫欄位
    }*/
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>todolist</title>
        <link rel="stylesheet" type="text/css" href="css/todolist.css">
        <link rel="stylesheet" type="text/css" href="css/topBar.css">
        <link href="https://fonts.googleapis.com/css?family=Droid+Sans|Roboto|Poppins|Varela+Round|Raleway" rel="stylesheet">
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">
    </head>
    
    <body>
        
        <div class="wrapper">
            <div class="top_bar">
                <ul>
                    <li><a href="index.php">Home</a></li>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li><a href="timer.php">Timer</a></li>
                    <li style="float:right"><a class="active" href="app/logout.php">Log out</a></li>
                </ul>
            </div>
        </div>
        <div class="todolist">
            <h1>to-do</h1>
            
                <?php if(mysqli_num_rows($results) != 0): /*如果有資料*/ ?>
                <ul>
                    <?php foreach($results as $item): ?>
                    <li>
                        <span class="todoitem">
                            <input type="checkbox" id="check<?php echo $item['listID'];?>" onclick="javascript:location.href='app/markTodo.php?id=<?php echo $item['listID']; ?>'"
                                <?php echo $item['done']? 'disabled="disabled" checked ' : '' /*if done, mark ad checked.*/?>
                            >
                            <label for="check<?php echo $item['listID'];?>"><?php echo $item['todo']; ?></label>
                                <a href='app/deleteTodo.php?id=<?php echo $item['listID']; ?>' class="delete_btn">remove</a>
                        </span>
                    </li>
                    <?php endforeach; ?>
                    <?php else: /*沒有資料時*/ ?>
                    <p>nothing to-do now.</p>
                </ul>
                <?php endif; ?>
            
            <form class="add-todo" action="app/addNewTodo.php" method="post">
                <input class="input-todo" type="text" name="new-todo" placeholder="add new to-do" autocomplete="off" required>
                   <!-- <input class="submit_btn" type="submit" value="+">-->
            </form>
        </div>
        
    </body>
</html>