<?php
    require_once 'app/header.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Raleway|Inconsolata|Droid+Sans|Roboto|Poppins|Varela+Round" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="css/register_form.css">
        <link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
        <script src="sweetalert/dist/sweetalert.min.js"></script>
        
        <script>
            function checkPassword()
            {
                var pass1 = document.getElementById("p1");
                var pass2 = document.getElementById("p2");
                if (pass1.value !== pass2.value) 
                {
                    pass2.setCustomValidity("wrong");
                }
                else
                {
                    pass2.setCustomValidity("");
                }
            }
            
            function resendEmail()
            {
                swal({
                    title: "Resend Email",
                    text: "input your registered e-mail address below",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "xxx@xxx.xx"
              },
              function(inputValue)
              {
                if (inputValue === false) return false;

                else if (inputValue === "") 
                {
                  swal.showInputError("can't be empty!");
                  return false;
                }
                else if (!validateEmail(inputValue))
                {
                    swal.showInputError("invalidate Email format");
                    return false;
                }
                else
                {
                    window.location.replace("?resend=" + inputValue);
                    //swal("OK!", "you will recieve the reset mail at " + inputValue + " soon.", "success");
                }
              });
            }
            
            function validateEmail(email) 
            {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
        </script>
    </head>
    
    <body>
        <div class="title">
            <h1>Create Account</h1>
        </div>
        
        <div class="register_form">
            <form action="app/registering.php" autocomplete="off" method="post">

                <input type="email" name="Email"
                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$"  required>
                <div class="label-text">E-mail</div>
                
                <input type="text" name="Username" 
                       pattern=".{3,}" maxlength="20" required>
                <div class="label-text">Username</div>
                
                <input id="p1" type="password" name="Password"  
                       pattern=".{5,}" maxlength="20" required>
                <div class="label-text">Password</div>

                <input id="p2" type="password" name="Password2" 
                       pattern=".{5,}" maxlength="20" onchange="checkPassword();" required>
                <div class="label-text">Password again</div>

                <button id="submit" type="submit">SUBMIT</button><br>
            </form>
            <a href="#" onclick="resendEmail()">resend email</a><br/>
        </div>
        
        <div class="background"></div>
        
        <?php
           if(filter_has_var(INPUT_GET, 'resend'))
           {
                $email = filter_input(INPUT_GET, 'resend');
                if($email != null)
                {
                    $rows = queryMysql("SELECT * FROM user WHERE email = '$email'");
                    if(mysqli_num_rows($rows) != 0)
                    {
                        $row = mysqli_fetch_assoc($rows);
                        if($row['isValid'] == 1)
                        {
                            echo "<script>swal('this email already verified', 'you can login now');</script> ";
                        }
                        else
                        {
                            $uid = $row['userID'];
                            $validRows = queryMysql("SELECT * FROM validation WHERE userID = '$uid'");
                            if(mysqli_num_rows($validRows) != 0)
                            {
                                $validRow = mysqli_fetch_assoc($validRows);
                                sendMailWithCode($email, $validRow['code']);
                                echo "<script>swal('Done', 'sent!');</script> ";
                            }
                            else
                            {
                                sendMail($email);
                                echo "<script>swal('Done', 'sent!');</script> ";
                            }
                        }
                        
                    }
                    else
                    {
                        echo "<script>swal('No such user', 'please register first');</script> ";
                    }
                }
           }
        ?>
    </body>
</html>
