<?php
    require_once 'app/header.php';
    $loginBtn = 'Login';
    $loginBtnLink = 'login_form.php';
    
    if($loggedIn == TRUE)
    {
        $loginBtn = $user;
        $loginBtnLink = 'app/logout.php';
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>index</title>
        <link rel="stylesheet" type="text/css" href="css/index.css">
        <link href="https://fonts.googleapis.com/css?family=Raleway|Inconsolata|Droid+Sans|Roboto|Poppins" rel="stylesheet"> 
        <link rel="image_src" href="img/burger.png">
    </head>
    <body>
        <div class="wrapper">
            
            <div class="top_bar">
                <ul>
                    <li><a href="#about">About</a></li>
                    <li><a href="#contact">Contact</a></li>
                    <li style="float:right"><a class="active" href="<?php echo $loginBtnLink; ?>"><?php echo $loginBtn; ?></a></li>
                </ul>
            </div>
            
            <div class="title">
                <?php if($loggedIn == FALSE) :?>
                <h1>Welcome to Pomodoro List</h1>
                <a href="register_form.php"><button>Sign up</button></a>
                <?php else: ?>
                <h1>What do you feel like doing today?</h1>
                <a href="todolist.php"><button>to-do</button></a>
                <a href="timer.php"><button>timer</button></a>
                <?php endif; ?>
                <!--<button type='button' onclick="location.href='register_form.html'">Sign up</button>-->
            </div>
            
            <div class="list_intro">
                <ul>
                    <li class="intro_form">
                        <div class="intro">
                            <img src="img/todolist.png" alt="todolist" style="width:32%;">
                            <div class="intro_text">
                                <h1>to-do</h1>
                                <p>
                                    This is a basic to-do list tool.<br/>
                                    You can manage your task anytime.
                                </p>
                            </div>
                        </div>
                    </li>

                    <li class="intro_form">
                        <div class="intro">

                            <img src="img/timer-02.png" alt="todolist" style="width:32%;">
                                <div class="intro_text">
                                <h1>Pomodoro</h1>
                                <p class="intro_p_position">
                                    Pomodoro is a time management tool, which use Pomodoro Technique.<br/>
                                    Focus 25 min, and take a break for 5 min.
                                </p>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </body>
</html>
