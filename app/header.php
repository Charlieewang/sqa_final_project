<?php
    session_start();
    require_once 'functions.php';
    
    
    if(isset($_SESSION['user']) &&  isset($_SESSION['userID']))
    {
        $user = $_SESSION['user'];
        $userID = $_SESSION['userID'];
        $loggedIn = TRUE;
    }
    else
    {
        $loggedIn = FALSE;
    }
  
?>