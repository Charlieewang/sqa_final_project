<?php

    require_once 'header.php';

    $Username = filter_input(INPUT_POST, 'Username');
    $Password = filter_input(INPUT_POST, 'Password');

    /*check user account*/
    $rows = queryMysql("SELECT * FROM user WHERE username = '$Username'");
    
    if(mysqli_num_rows($rows) != 0)
    {
        $row = mysqli_fetch_assoc($rows);
        if(password_verify($Password, $row['password']))
        {
            if($row['isValid'] == 1)
            {
                $_SESSION['user'] = $Username;
                $_SESSION['userID'] = $row['userID'];
                header("Location: ../index.php");
            }
            else
            {
                header("Location: ../login_form.php?loginfailed=2");// not verify email yet
            }
        }
        else//wrong password
        {
            header("Location: ../login_form.php?loginfailed=0"); // change to login.php?loginfailed=0
        }
    }

    else//wrong username
    {
        header("Location: ../login_form.php?loginfailed=1"); // change to login.php?loginfailed=1
    }

