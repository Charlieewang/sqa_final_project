<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <title>Pomodoro Timer</title>
    <link rel="stylesheet" type="text/css" href="css/timer.css">
    <link rel="stylesheet" type="text/css" href="css/topBar.css">
    <link href="https://fonts.googleapis.com/css?family=Jaldi|Khand|Raleway" rel="stylesheet">
    <script src="js/timer.js"></script>
</head>
<body>
    
    <div class="wrapper">
        <div class="top_bar">
            <ul>
                <li><a href="index.php">Home</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#contact">Contact</a></li>
                <li><a href="todolist.php">To-do</a></li>
                <li style="float:right"><a class="active" href="app/logout.php">Log out</a></li>
            </ul>
        </div>
    </div>
    <div class="timer_class">
        <input id="min" type="text" maxlength="2" value="25">
    <!--    <p id="min">00</p>-->
        <p id="colon">:</p>
        <input id="sec" type="text" maxlength="2" value="00">
    <!--    <p id="sec">02</p>-->

    <!--    <p id="timer">00:02</p>-->
        <br>
        <button id="control_btn" onclick="changeState()">&#9654;</button>
        <button id="control_btn" onclick="reset()">&#8634;</button>
    </div>
</body>
</html>
