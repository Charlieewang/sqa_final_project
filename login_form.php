<?php
    require_once 'app/header.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link href="https://fonts.googleapis.com/css?family=Raleway|Poppins" rel="stylesheet"> 
        <link rel="stylesheet" type="text/css" href="css/login_form.css">
        
        <link rel="stylesheet" type="text/css" href="sweetalert/dist/sweetalert.css">
        <script src="sweetalert/dist/sweetalert.min.js"></script>
        
        <script>
            function help()
            {
                swal({
                    title: "Forgot Password?",
                    text: "input your e-mail address below",
                    type: "input",
                    showCancelButton: true,
                    closeOnConfirm: false,
                    animation: "slide-from-top",
                    inputPlaceholder: "xxx@xxx.xx"
              },
              function(inputValue)
              {
                if (inputValue === false) return false;

                else if (inputValue === "") 
                {
                  swal.showInputError("can't be empty!");
                  return false;
                }
                else if (!validateEmail(inputValue))
                {
                    swal.showInputError("invalidate Email format");
                    return false;
                }
                else
                {
                    //swal("OK!", "you will recieve the reset mail at " + inputValue + " soon.", "success");
                    window.location.replace("?forgot_password=" + inputValue);
                    //window.location.href="#?email=" + inputValue;
                }
              });
            }
            
            function validateEmail(email) 
            {
                var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(email);
            }
            
            function login_failed($input)
            {
                
                swal({
                title: "Login Failed",
                text: "wrong, " + $input,
                type: "warning",
                showCancelButton: false,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Try again!",
                closeOnConfirm: false
              },
              function()
              {
                window.location.href = 'login_form.php';
              });
            }
            
        </script>
    </head>
    
    <body>
        <div class="title">
            <h1>Welcome back</h1>
        </div>
        
        <div class="login_form">
            <form action="app/login_process.php" autocomplete="off" method="post">
                
                <input type="text" name="Username" pattern=".{3,}" maxlength="20" required>
                <div class="label-text">Username</div>

                <input type="password" name="Password" pattern=".{5,}" maxlength="20" required>
                <div class="label-text">Password</div>

                <button id="submit" type="submit">ENTER</button><br>
            </form>
            <a href="#" onclick="help()">need help ?</a><br/>
        </div>
        <div class="background"></div>
    </body>
    <?php
        if(filter_has_var(INPUT_GET, 'loginfailed'))
        {
            $failFlag = (filter_input(INPUT_GET, 'loginfailed')? : $_GET["loginfailed"] = null );
            if($failFlag == 0)
            {
                echo "<script>login_failed('password does not match.');</script>";
            }
            else if($failFlag == 1)
            {
                echo "<script>login_failed('username not exist.');</script>";
            }
            else if($failFlag == 2)
            {
                echo "<script>login_failed('email not verify yet.');</script>";
            }
            else
            {
                echo "<script>login_failed('unexpected error. please contact the site manager');</script>";
            }
        }
        else if(filter_has_var(INPUT_GET, 'forgot_password'))
        {
            $email = filter_input(INPUT_GET, 'forgot_password');
            if($email != null)
            {
                $rows = queryMysql("SELECT * FROM user WHERE email = '$email'");
                if(mysqli_num_rows($rows) != 0)//user exist
                {
                    $row = mysqli_fetch_assoc($rows);
                    $uid = $row['userID'];
                    $tempRan = generateRandomString(20);
                    queryMysql("INSERT INTO forgot_password_request (userID, code) values('$uid', '$tempRan')");
                    sendMailWithCode($email, $tempRan);
                    echo "<script>swal('Sent', 'please check your email');</script>";
                }
                else
                {
                    echo "<script>swal('No such user', 'please check your input');</script>";
                }
            }
        }
    ?>
    
</html>
